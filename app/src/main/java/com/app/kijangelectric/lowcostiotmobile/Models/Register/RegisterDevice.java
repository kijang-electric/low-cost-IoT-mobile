package com.app.kijangelectric.lowcostiotmobile.Models.Register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterDevice {
    public RegisterDevice(String username, String serial){
        this.serial = serial;
        this.username = username;
    }

    @SerializedName("serial")
    @Expose
    private String serial;
    @SerializedName("username")
    @Expose
    private String username;

    public String getSerial() { return serial; }

    public void setSerial(String serial) { this.serial = serial; }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
