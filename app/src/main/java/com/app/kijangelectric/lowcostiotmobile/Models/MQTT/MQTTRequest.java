package com.app.kijangelectric.lowcostiotmobile.Models.MQTT;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MQTTRequest {
    public MQTTRequest(String serial_device) {
        this.serial_device = serial_device;
    }

    @SerializedName("serial_device")
    @Expose
    private String serial_device;

    public String getSerial_device() {
        return serial_device;
    }

    public void setSerial_device(String serial_device) {
        this.serial_device = serial_device;
    }
}
