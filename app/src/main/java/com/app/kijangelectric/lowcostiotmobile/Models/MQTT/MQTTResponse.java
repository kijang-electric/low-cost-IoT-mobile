package com.app.kijangelectric.lowcostiotmobile.Models.MQTT;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MQTTResponse {
    @SerializedName("mqtt_host")
    @Expose
    private String mqtt_host;
    @SerializedName("mqtt_user")
    @Expose
    private String mqtt_user;
    @SerializedName("mqtt_password")
    @Expose
    private String mqtt_password;
    @SerializedName("mqtt_port")
    @Expose
    private String mqtt_port;
    @SerializedName("mqtt_tls")
    @Expose
    private String mqtt_tls;
    @SerializedName("mqtt_topic")
    @Expose
    private String mqtt_topic;

    public String getMqtt_host() {
        return mqtt_host;
    }

    public void setMqtt_host(String mqtt_host) {
        this.mqtt_host = mqtt_host;
    }

    public String getMqtt_user() {
        return mqtt_user;
    }

    public void setMqtt_user(String mqtt_user) {
        this.mqtt_user = mqtt_user;
    }

    public String getMqtt_password() {
        return mqtt_password;
    }

    public void setMqtt_password(String mqtt_password) {
        this.mqtt_password = mqtt_password;
    }

    public String getMqtt_port() {
        return mqtt_port;
    }

    public void setMqtt_port(String mqtt_port) {
        this.mqtt_port = mqtt_port;
    }

    public String getMqtt_tls() {
        return mqtt_tls;
    }

    public void setMqtt_tls(String mqtt_tls) {
        this.mqtt_tls = mqtt_tls;
    }

    public String getMqtt_topic() {
        return mqtt_topic;
    }

    public void setMqtt_topic(String mqtt_topic) {
        this.mqtt_topic = mqtt_topic;
    }
}
