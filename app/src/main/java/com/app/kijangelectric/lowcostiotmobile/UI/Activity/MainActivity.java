package com.app.kijangelectric.lowcostiotmobile.UI.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.app.kijangelectric.lowcostiotmobile.API.APIClient;
import com.app.kijangelectric.lowcostiotmobile.API.APIInterface;
import com.app.kijangelectric.lowcostiotmobile.Base.BaseActivity;
import com.app.kijangelectric.lowcostiotmobile.Models.User.UserRequest;
import com.app.kijangelectric.lowcostiotmobile.Models.User.UserResponse;
import com.app.kijangelectric.lowcostiotmobile.R;
import com.app.kijangelectric.lowcostiotmobile.SessionManager.UserSession;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.greetingMsg) TextView greetingMsg;
    private ProgressDialog mRegProgres;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bind(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mRegProgres = new ProgressDialog(this);

        UserSession session = new UserSession(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        final TextView txtProfileName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.userNameKu);
        final TextView txtProfileEmail = (TextView) navigationView.getHeaderView(0).findViewById(R.id.emailUserKu);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<UserResponse> call = apiInterface.requestUser(new UserRequest(session.getUserName()));
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(response.isSuccessful()) {
                    mRegProgres.dismiss();

                    greetingMsg.setText("Selamat Datang " + response.body().getFirstName() + " " + response.body().getLastName());
                    txtProfileName.setText(response.body().getFirstName() + " " + response.body().getLastName());
                    txtProfileEmail.setText(response.body().getEmail());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                mRegProgres.dismiss();
                Toast.makeText(MainActivity.this, "Error : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_welcome) {
            // Handle the camera action
        } else if (id == R.id.nav_doc) {

        } else if (id == R.id.nav_monlist) {

        } else if (id == R.id.nav_moncard) {

        } else if (id == R.id.nav_listdev) {

        } else if (id == R.id.nav_regdev) {

        } else if (id == R.id.nav_profile) {

        } else if (id == R.id.nav_logout) {
            UserSession session = new UserSession(this);
            session.logoutUser();
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
