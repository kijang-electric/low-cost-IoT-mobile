package com.app.kijangelectric.lowcostiotmobile.UI.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.app.kijangelectric.lowcostiotmobile.Base.BaseActivity;
import com.app.kijangelectric.lowcostiotmobile.Models.Global.GlobalResponse;
import com.app.kijangelectric.lowcostiotmobile.Models.Register.RegisterUser;
import com.app.kijangelectric.lowcostiotmobile.R;
import com.app.kijangelectric.lowcostiotmobile.API.APIClient;
import com.app.kijangelectric.lowcostiotmobile.API.APIInterface;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterUserActivity extends BaseActivity {
    @BindView(R.id.txtUsername)TextInputEditText txtUsename;
    @BindView(R.id.txtFirstName)TextInputEditText txtFirstName;
    @BindView(R.id.txtLastName)TextInputEditText txtLastName;
    @BindView(R.id.txtEmail)TextInputEditText txtEmail;
    @BindView(R.id.txtPassword)TextInputEditText txtPassword;
    @BindView(R.id.btnRegister)Button btnRegister;
    private ProgressDialog mRegProgres;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bind(R.layout.activity_register_user);
        setTitle("Register");
        mRegProgres = new ProgressDialog(this);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fieldCheck() == true ){
                    mRegProgres.setTitle("Getting Data");
                    mRegProgres.setMessage("Please Wait...");
                    mRegProgres.setCanceledOnTouchOutside(false);
                    mRegProgres.show();
                    String username = txtUsename.getText().toString().trim();
                    String first_name = txtFirstName.getText().toString().trim();
                    String last_name = txtLastName.getText().toString().trim();
                    String email = txtEmail.getText().toString().trim();
                    String password = txtPassword.getText().toString().trim();
                    APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                    Call<GlobalResponse> call = apiInterface.registerUser(new RegisterUser(username,first_name,last_name,email,password));
                    call.enqueue(new Callback<GlobalResponse>() {
                        @Override
                        public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                            if(response.body().getSuccess() == true){
                                Toast.makeText(RegisterUserActivity.this, response.body().getInfo(), Toast.LENGTH_SHORT).show();
                                mRegProgres.dismiss();
                                finish();
                            }else{
                                mRegProgres.dismiss();
                                Toast.makeText(RegisterUserActivity.this, response.body().getInfo(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<GlobalResponse> call, Throwable t) {
                            mRegProgres.dismiss();
                            Toast.makeText(RegisterUserActivity.this, "Error : " + t, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }
    private Boolean fieldCheck(){
        Boolean fieldCheck = true;

        if(TextUtils.isEmpty(txtUsename.getText())){
            txtUsename.setError("Insert this Field!");
            fieldCheck = false;
        }
        if(TextUtils.isEmpty(txtFirstName.getText())){
            txtFirstName.setError("Insert this Field!");
            fieldCheck = false;
        }
        if(TextUtils.isEmpty(txtLastName.getText())){
            txtLastName.setError("Insert this Field!");
            fieldCheck = false;
        }
        if(TextUtils.isEmpty(txtEmail.getText())){
            txtEmail.setError("Insert this Field!");
            fieldCheck = false;
        }
        if(TextUtils.isEmpty(txtPassword.getText())){
            txtPassword.setError("Insert this Field!");
            fieldCheck = false;
        }

        return fieldCheck;
    }
}
