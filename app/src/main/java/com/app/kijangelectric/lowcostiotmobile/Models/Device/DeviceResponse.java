package com.app.kijangelectric.lowcostiotmobile.Models.Device;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceResponse {
    @SerializedName("serial")
    @Expose
    private String serial;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("product")
    @Expose
    private String product;

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}
