package com.app.kijangelectric.lowcostiotmobile.API;

import retrofit2.Call;

import com.app.kijangelectric.lowcostiotmobile.Models.Device.DeviceRequest;
import com.app.kijangelectric.lowcostiotmobile.Models.Device.DeviceResponse;
import com.app.kijangelectric.lowcostiotmobile.Models.Global.GlobalResponse;
import com.app.kijangelectric.lowcostiotmobile.Models.Login.LoginRequest;
import com.app.kijangelectric.lowcostiotmobile.Models.Login.LoginResponse;
import com.app.kijangelectric.lowcostiotmobile.Models.MQTT.MQTTRequest;
import com.app.kijangelectric.lowcostiotmobile.Models.MQTT.MQTTResponse;
import com.app.kijangelectric.lowcostiotmobile.Models.Register.RegisterDevice;
import com.app.kijangelectric.lowcostiotmobile.Models.Register.RegisterUser;
import com.app.kijangelectric.lowcostiotmobile.Models.User.UserRequest;
import com.app.kijangelectric.lowcostiotmobile.Models.User.UserResponse;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIInterface {
    @GET("api/test")
    Call<GlobalResponse> getTestStatus();

    @POST("api/login")
    Call<LoginResponse> getLoginStatus(@Body LoginRequest loginRequest);

    @POST("api/reg-user")
    Call<GlobalResponse> registerUser (@Body RegisterUser userRegister);

    @POST("api/detail-user")
    Call<UserResponse> requestUser (@Body UserRequest user);

    @POST("api/reg-device")
    Call<GlobalResponse> registerDevice (@Body RegisterDevice device);

    @POST("api/all-device")
    Call<DeviceResponse> allDevice (@Body DeviceRequest device);

    @POST("api/detail-device")
    Call<DeviceResponse> detailDevice (@Body DeviceRequest device);

    @POST("api/mon-details")
    Call<MQTTResponse> monDetails (@Body MQTTRequest mqtt);
}