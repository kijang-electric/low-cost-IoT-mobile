package com.app.kijangelectric.lowcostiotmobile.SessionManager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.app.kijangelectric.lowcostiotmobile.UI.Activity.LoginActivity;
import com.app.kijangelectric.lowcostiotmobile.UI.Activity.MainActivity;

public class UserSession {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context mContext;
    public static final String PREFER_NAME = "PrefIot";
    public static final String IS_USER_LOGIN = "LoginStatus";
    public static final String KEY_USER_ID = "id_user";


    public UserSession(Context mContext) {
        this.mContext = mContext;
        preferences = mContext.getSharedPreferences(PREFER_NAME,0);
        editor = preferences.edit();
    }

    public void setLoginSession(String userid){
        editor.putBoolean(IS_USER_LOGIN,true);
        editor.putString(KEY_USER_ID, userid);
        editor.commit();
    }

    public boolean checkLogin(){
        if(!this.isUserLoggedIn()){
            Intent i = new Intent(mContext, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(i);
            return true;
        }
        return false;
    }

    public void logoutUser(){
        editor.clear();
        editor.commit();

        Intent i = new Intent(mContext, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        mContext.startActivity(i);
    }

    public int getUserID(){
        return preferences.getInt(KEY_USER_ID,0);
    }
    public String getUserName() {
        return preferences.getString(KEY_USER_ID, String.valueOf(0));
    }
    public boolean isUserLoggedIn(){
        return preferences.getBoolean(IS_USER_LOGIN, false);
    }
}
