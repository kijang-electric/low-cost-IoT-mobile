package com.app.kijangelectric.lowcostiotmobile.UI.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.kijangelectric.lowcostiotmobile.API.APIClient;
import com.app.kijangelectric.lowcostiotmobile.API.APIInterface;
import com.app.kijangelectric.lowcostiotmobile.Base.BaseActivity;
import com.app.kijangelectric.lowcostiotmobile.Models.Login.LoginRequest;
import com.app.kijangelectric.lowcostiotmobile.Models.Login.LoginResponse;
import com.app.kijangelectric.lowcostiotmobile.R;
import com.app.kijangelectric.lowcostiotmobile.SessionManager.UserSession;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {
    @BindView(R.id.txtLoginEmail) TextInputEditText txtLoginEmail;
    @BindView(R.id.txtLoginPassword) TextInputEditText txtLoginPassword;
    @BindView(R.id.txtCreateAcc) TextView txtCreateAcc;
    @BindView(R.id.txtTestGet) TextView txtTestGet;
    @BindView(R.id.btnLogin) Button btnLogin;
    private ProgressDialog mRegProgres;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bind(R.layout.activity_login);
        mRegProgres = new ProgressDialog(this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRegProgres.setTitle("Getting Data");
                mRegProgres.setMessage("Please Wait...");
                mRegProgres.setCanceledOnTouchOutside(false);
                mRegProgres.show();
                String email = txtLoginEmail.getText().toString().trim();
                String pass = txtLoginPassword.getText().toString().trim();
                APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                Call<LoginResponse> call = apiInterface.getLoginStatus(new LoginRequest(email,pass));
                call.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        if(response.body().getSuccess() == true){
                            mRegProgres.dismiss();
                            UserSession userSession = new UserSession(LoginActivity.this);
                            userSession.setLoginSession(response.body().getIdUser());
                            Intent i = new Intent(LoginActivity.this, MainActivity.class);

                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            startActivity(i);
                            finish();
                        }else{
                            mRegProgres.dismiss();
                            Toast.makeText(LoginActivity.this, response.body().getInfo(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        mRegProgres.dismiss();
                        Toast.makeText(LoginActivity.this, "Login Failed : "+ t, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        txtCreateAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,RegisterUserActivity.class));
            }
        });
        txtTestGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,TestGetDataActivity.class));
            }
        });
    }
}
