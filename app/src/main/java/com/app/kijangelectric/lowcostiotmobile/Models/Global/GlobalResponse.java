package com.app.kijangelectric.lowcostiotmobile.Models.Global;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GlobalResponse {
    @SerializedName("Success")
    @Expose
    private Boolean success;
    @SerializedName("Info")
    @Expose
    private String info;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
