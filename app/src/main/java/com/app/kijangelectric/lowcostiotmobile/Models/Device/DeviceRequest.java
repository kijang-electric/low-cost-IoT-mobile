package com.app.kijangelectric.lowcostiotmobile.Models.Device;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceRequest {
    public DeviceRequest(String username){
        this.username = username;
    }

    @SerializedName("username")
    @Expose
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
