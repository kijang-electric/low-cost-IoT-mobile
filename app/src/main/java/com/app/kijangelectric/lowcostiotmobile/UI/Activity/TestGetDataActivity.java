package com.app.kijangelectric.lowcostiotmobile.UI.Activity;

import android.os.Bundle;
import android.widget.TextView;

import com.app.kijangelectric.lowcostiotmobile.API.APIClient;
import com.app.kijangelectric.lowcostiotmobile.API.APIInterface;
import com.app.kijangelectric.lowcostiotmobile.Base.BaseActivity;
import com.app.kijangelectric.lowcostiotmobile.Models.Global.GlobalResponse;
import com.app.kijangelectric.lowcostiotmobile.R;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestGetDataActivity extends BaseActivity{
    @BindView(R.id.txtGetApiData)
    TextView txtGetApiData;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bind(R.layout.activity_test_get_data);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<GlobalResponse> call = apiInterface.getTestStatus();
        call.enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                String info = response.body().getInfo();
                Boolean status = response.body().getSuccess();

                txtGetApiData.setText(info + status);
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                showToast("Error" + t);
            }
        });
    }
}
